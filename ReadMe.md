# Jfire-Simplerpc

高性能 简单

---
##介绍
Jfire-Simplerpc是一个非常简单但是却性能强大的rpc框架。使用的是自行开发的序列化算法（java领域内性能最强的fose序列化框架，已获得专利受理），自行开发的基于AIO的服务器框架，两者合一再结合动态代码编程而成的最终产品。只需要5行代码就可以启动一个rpc服务器。只需要一行代码就可以进行rpc调用。没有任何束缚。

由于在序列化算法，服务器框架算法等方面均采用了最为强大的框架，使得整体性能十分的优秀。

##快速入门
###无加密调用
首先来看看在非加密模式下，怎么启动一个rpc服务器
```java
RcConfig rcConfig = new RcConfig();//创建一个配置对象
rcConfig.setPort(1688);//设定服务器的监听端口是1688
rcConfig.setProxyNames("print"); //设定对外提供的rpc服务的名称
rcConfig.setImpls(new PrintImpl());//设定对外提供的rpc服务的类的实例
rcServer = new RcServer(rcConfig);
rcServer.start();//启动服务器，开始监听。
```
服务端的代码只有一句话。
```java
   Print print = RpcFactory.getProxy("print", Print.class, "127.0.0.1", 1688);//获取rpc调用的客户端。其中Print.class是一个接口，定义的方法签名和服务端一致即可。相同签名的方法就可以调用到服务端上的方法实现并且返回对应的数据。该客户端实例是多线程安全的，所以生成之后可以在任何地方使用。
```
###加密调用
SimpleRpc支持加密调用。方式类似SSL，握手加密采用RSA1024位加密，完成握手时会使用rsa加密传递通讯加密密钥，通讯加密采用AES256位加密。其中通讯加密密钥是每一次握手时随机生成。基于这样的方式，通讯加密几乎不可能被破解。
如下是服务端代码
```java
RcConfig serverConfig = new RcConfig();
serverConfig.setPort(1688);
serverConfig.setPrivateKey(privateKey);//设置服务端的私钥。这个私钥是一个byte数组，代表密钥
serverConfig.setProxyNames("print");
serverConfig.setImpls(new PrintImpl());
serverConfig.setWaitTimeout(4000);
serverConfig.activeAuth();//开启加密机制。
authRcServer = new RcServer(serverConfig);
authRcServer.start();
```
客户端代码也会稍微多一些参数
```java
 Print print = RpcFactory.getAesProxy(Print.class, "print", publicKey, "127.0.0.1", 1688); //比起无加密的方式，多了一个公钥的参数。
```



