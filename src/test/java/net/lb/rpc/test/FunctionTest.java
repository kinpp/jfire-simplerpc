package net.lb.rpc.test;

import link.jfire.baseutil.simplelog.ConsoleLogFactory;
import link.jfire.baseutil.simplelog.Logger;
import link.jfire.simplerpc.client.RpcFactory;
import link.jfire.simplerpc.exception.NoSuchMethodException;
import link.jfire.simplerpc.server.RcConfig;
import link.jfire.simplerpc.server.RcServer;
import link.jfire.testsupport.rule.CustomRule;
import net.lb.rpc.test.data.ComplexOPbject;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

public class FunctionTest
{
    private static RcServer rcServer;
    private Logger          logger = ConsoleLogFactory.getLogger();
    @Rule
    public CustomRule       rule   = new CustomRule();
    
    @BeforeClass
    public static void before()
    {
        RcConfig rcConfig = new RcConfig();
        rcConfig.setPort(1688);
        rcConfig.setProxyNames("print");
        rcConfig.setImpls(new PrintImpl());
        rcServer = new RcServer(rcConfig);
        rcServer.start();
    }
    
    @AfterClass
    public static void after()
    {
        rcServer.stop();
    }
    
    @Test
    public void methodWithoutReturn()
    {
        Print print = RpcFactory.getProxy("print", Print.class, "127.0.0.1", 1688);
        print.methodWithoutReturn("没有方法返回值");
    }
    
    @Test
    public void methodWithReturn()
    {
        Print print = RpcFactory.getProxy("print", Print.class, "127.0.0.1", 1688);
        String result = print.methodWithReturn("有方法返回值");
        Assert.assertEquals("有方法返回值追加的末尾信息", result);
    }
    
    Print print = RpcFactory.buildProxyConfig(Print.class).setProxyName("print").setIp("127.0.0.1").setPort(1688).getProxy();
    
    @Test
    public void returnComplexOPbject()
    {
        ComplexOPbject param = new ComplexOPbject();
        param.setAge(12);
        param.setName("林斌");
        param.setSex(2);
        print.returnComplexOPbject(param);
    }
    
    @Test
    public void wrongMethodName()
    {
        Print2 print2 = RpcFactory.getProxy("print", Print2.class, "127.0.0.1", 1688);
        try
        {
            print2.methodNotExist();
        }
        catch (Exception e)
        {
            e.getCause().printStackTrace();
            Assert.assertTrue(e.getCause().getCause().getCause() instanceof NoSuchMethodException);
        }
    }
    
    @Ignore
    @Test
    // @MutiThreadTest(repeatTimes = 20, threadNums = 800)
    // @RepeatTest(1000000)
    public void testFotTimes() throws InterruptedException
    {
        ComplexOPbject param = new ComplexOPbject();
        param.setAge(12);
        param.setName("林斌");
        param.setSex(2);
        print.returnComplexOPbject(param);
        // ((CloseConnect) print).closeConnect();
        // Thread.sleep(400);
    }
}
