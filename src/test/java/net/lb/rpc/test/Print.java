package net.lb.rpc.test;

import net.lb.rpc.test.data.ComplexOPbject;

public interface Print
{
    public void methodWithoutReturn(String param);
    
    public void par(String[][] ps);
    
    public String methodWithReturn(String param);
    
    public Object[] returnComplexOPbject(ComplexOPbject complexOPbject);
}
