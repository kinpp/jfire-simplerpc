package net.lb.rpc.test.demo;

import javassist.CannotCompileException;
import javassist.ClassClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;
import link.jfire.baseutil.simplelog.ConsoleLogFactory;
import link.jfire.baseutil.simplelog.Logger;
import link.jfire.simplerpc.server.invoke.ProxyBuilder;
import net.lb.rpc.test.Print;

public class Demo
{
    private static ClassPool classPool = ClassPool.getDefault();
    private static Logger    logger    = ConsoleLogFactory.getLogger();
    
    static
    {
        ClassPool.doPruning = true;
        classPool.insertClassPath(new ClassClassPath(ProxyBuilder.class));
        classPool.importPackage("link.jfire.socket");
        classPool.importPackage("link.jfire.simplerpc");
        classPool.importPackage("java.util");
        classPool.importPackage("net.lb.rpc.test");
    }
    
    public static void run() throws CannotCompileException, NotFoundException, InstantiationException, IllegalAccessException
    {
        CtClass perClass = classPool.get("net.lb.rpc.test.PrintImpl");
        CtMethod method = perClass.getDeclaredMethod("par");
        method.insertBefore("System.out.println($sig[0]);");
        Print print = (Print) perClass.toClass().newInstance();
        print.par(null);
    }
    
    public static void main(String[] args) throws InstantiationException, IllegalAccessException, CannotCompileException, NotFoundException
    {
        Demo.run();
    }
}
