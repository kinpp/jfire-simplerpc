package net.lb.rpc.test.auth;

import link.jfire.baseutil.StringUtil;
import link.jfire.baseutil.simplelog.ConsoleLogFactory;
import link.jfire.baseutil.simplelog.Logger;
import link.jfire.codejson.JsonTool;
import link.jfire.simplerpc.client.CloseConnect;
import link.jfire.simplerpc.client.RpcFactory;
import link.jfire.simplerpc.exception.NoSuchMethodException;
import link.jfire.simplerpc.server.RcConfig;
import link.jfire.simplerpc.server.RcServer;
import link.jfire.testsupport.rule.CustomRule;
import link.jfire.testsupport.rule.RepeatTest;
import net.lb.rpc.test.Print;
import net.lb.rpc.test.Print2;
import net.lb.rpc.test.PrintImpl;
import net.lb.rpc.test.data.ComplexOPbject;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

public class AuthFunctionTest
{
    private static RcServer authRcServer;
    private static byte[]   privateKey = StringUtil.hexStringToBytes("30820277020100300d06092a864886f70d0101010500048202613082025d02010002818100c142a72e01ec900d5aa4424918eed5edda36cb42c747404dc1dac91cdbed50040d2b5b0129fedbcfee0c6de90669d5541cd6b136b7dc112d267888c7e6a6033d4dffadaa68343ecd11e5e2164ca90e20be5ac972db8f832d466ba95b1521572ccbb0e304babb7c0f1c6489eef90f44915fa5f8d7af8a3f5360f10cad19471def0203010001028180356cbc947e59ac14d4fad58c298554c454ef92cf1a11bb58037ffcdaaa7d770237b5c58993d76d6294b700e74b2b949a950250a904b8a452ebf0a43746ae9bf42a13191ef5707e17ad91254857fe9293b822ec005acc19a6af13e56db22db4ff77b2809a8f5b73f9bf1444a80853711fe6a2abb755e89873937e0ecaca3bd599024100f3598418bcdc9f37368a4f28effb584139849163d897af35a460b0b13ef5a5bcb9c07320c25355023f7d4a9468a9edf6594d873e57cf8976981fdd79870e1aad024100cb4e8d217e95c2e171eef851e1b05f7d71aabce4f662b7cb5dc8f273aa92a1621974fd67c3d03b7003d510bf5ed1de0a5548a7b7de9e6e30d52c185e713f6a8b024100939b108d10c1a1b193db80f88bd821482ee3426d15cbabf315a9b08352751dfac3910044369ae54d5fc5579f7d7cd5623ff8ceb7f9fb609e16d7fd36e1bca46102404d1132c88e9ea0a82b3346286be01ae287930c55d2dc2b05c940623fdc5a071913c35847dc5f74188b2b5a54e0d1b754bd6d743e6c022c8d1051cdcf0728ddef024100dabbe4a649b484236258ff5255be24311f9606385453ebf7c77d14574b9b9626b65e0235c452b2db14e5ea498628c78656960863b93cea84567e75dd40777721");
    private static byte[]   publicKey  = StringUtil.hexStringToBytes("30819f300d06092a864886f70d010101050003818d0030818902818100c142a72e01ec900d5aa4424918eed5edda36cb42c747404dc1dac91cdbed50040d2b5b0129fedbcfee0c6de90669d5541cd6b136b7dc112d267888c7e6a6033d4dffadaa68343ecd11e5e2164ca90e20be5ac972db8f832d466ba95b1521572ccbb0e304babb7c0f1c6489eef90f44915fa5f8d7af8a3f5360f10cad19471def0203010001");
    private Logger          logger     = ConsoleLogFactory.getLogger();
    @Rule
    public CustomRule       rule       = new CustomRule();
    
    @BeforeClass
    public static void before()
    {
        RcConfig serverConfig = new RcConfig();
        serverConfig.setPort(1688);
        serverConfig.setPrivateKey(privateKey);
        serverConfig.setProxyNames("print");
        serverConfig.setImpls(new PrintImpl());
        serverConfig.setWaitTimeout(4000);
        serverConfig.activeAuth();
        authRcServer = new RcServer(serverConfig);
        authRcServer.start();
    }
    
    @AfterClass
    public static void after()
    {
        authRcServer.stop();
    }
    
    @Test
    public void methodWithoutReturn()
    {
        Print print = RpcFactory.getAesProxy(Print.class, "print", publicKey, "127.0.0.1", 1688);
        print.methodWithoutReturn("没有方法返回值");
    }
    
    @Test
    public void methodWithReturn()
    {
        Print print = RpcFactory.getAesProxy(Print.class, "print", publicKey, "127.0.0.1", 1688);
        String result = print.methodWithReturn("有方法返回值");
        Assert.assertEquals("有方法返回值追加的末尾信息", result);
    }
    
    Print printx = RpcFactory.getAesProxy(Print.class, "print", publicKey, "127.0.0.1", 1688);
    
    @Test
    @RepeatTest(100)
    public void returnComplexOPbject()
    {
        ComplexOPbject param = new ComplexOPbject();
        param.setAge(12);
        param.setName("林斌");
        param.setSex(2);
        Object[] result = printx.returnComplexOPbject(param);
        logger.info(JsonTool.toString(result));
    }
    
    @Test
    public void wrongMethodName()
    {
        Print2 print2 = RpcFactory.getAesProxy(Print2.class, "print", publicKey, "127.0.0.1", 1688);
        try
        {
            print2.methodNotExist();
        }
        catch (Exception e)
        {
            Assert.assertTrue(e.getCause().getCause() instanceof NoSuchMethodException);
        }
    }
    
    Print print = RpcFactory.getAesProxy(Print.class, "print", publicKey, "127.0.0.1", 1688);
    
    @Test
    // @MutiThreadTest(repeatTimes = 20, threadNums = 800)
    @Ignore
    public void testForTimes() throws InterruptedException
    {
        ComplexOPbject param = new ComplexOPbject();
        param.setAge(12);
        param.setName("林斌");
        param.setSex(2);
        print.returnComplexOPbject(param);
        ((CloseConnect) print).closeConnect();
        Thread.sleep(400);
    }
}
