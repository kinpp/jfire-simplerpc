package link.jfire.simplerpc.client;

import link.jfire.socket.socketclient.AesClient;
import link.jfire.socket.socketclient.Client;
import link.jfire.socket.socketclient.filter.DataFilter;
import link.jfire.socket.socketserver.util.HeadFactory;

public class AesCodeInvoker extends BytecodeInvoker
{
    private byte[] publicKey;
    
    /**
     * 使用参数初始化客户端调用代理，并且使用des进行加解密
     * 
     * @param proxyName
     * @param ip
     * @param port
     * @param privateKey
     * @param readTimeout
     * @param reuseChannelTimeout
     * @param dataFilters
     */
    public AesCodeInvoker(final byte[] publicKey)
    {
        this.publicKey = publicKey;
        clientChanelLocal = new ThreadLocal<Client>() {
            protected Client initialValue()
            {
                AesClient aesClient = new AesClient(publicKey, RpcResult.getInstance());
                aesClient.setIp(ip).setPort(port).setReadTimeout(readTimeout).setReuseChannelTimeout(reuseChannelTimeout).setDataFilters(dataFilters).setHeadFactory(headFactory);
                return aesClient;
            }
        };
    }
    
    public AesCodeInvoker setDataFilters(DataFilter... dataFilters)
    {
        if (dataFilters == null)
        {
            this.dataFilters = new DataFilter[0];
        }
        else
        {
            this.dataFilters = dataFilters;
        }
        clientChanelLocal = new ThreadLocal<Client>() {
            protected Client initialValue()
            {
                AesClient aesClient = new AesClient(publicKey, RpcResult.getInstance());
                aesClient.setIp(ip).setPort(port).setReadTimeout(readTimeout).setReuseChannelTimeout(reuseChannelTimeout).setDataFilters(AesCodeInvoker.this.dataFilters).setHeadFactory(headFactory);
                return aesClient;
            }
        };
        return this;
    }
    
    public AesCodeInvoker setReadTimeout(final long readTimeout)
    {
        this.readTimeout = readTimeout;
        clientChanelLocal = new ThreadLocal<Client>() {
            protected Client initialValue()
            {
                AesClient aesClient = new AesClient(publicKey, RpcResult.getInstance());
                aesClient.setIp(ip).setPort(port).setReadTimeout(readTimeout).setReuseChannelTimeout(reuseChannelTimeout).setDataFilters(AesCodeInvoker.this.dataFilters).setHeadFactory(headFactory);
                return aesClient;
            }
        };
        return this;
    }
    
    public AesCodeInvoker setReuseChannelTimeout(final long reuseChannelTimeout)
    {
        this.reuseChannelTimeout = reuseChannelTimeout;
        clientChanelLocal = new ThreadLocal<Client>() {
            protected Client initialValue()
            {
                AesClient aesClient = new AesClient(publicKey, RpcResult.getInstance());
                aesClient.setIp(ip).setPort(port).setReadTimeout(readTimeout).setReuseChannelTimeout(reuseChannelTimeout).setDataFilters(AesCodeInvoker.this.dataFilters).setHeadFactory(headFactory);
                return aesClient;
            }
        };
        return this;
    }
    
    public AesCodeInvoker setIp(final String ip)
    {
        this.ip = ip;
        clientChanelLocal = new ThreadLocal<Client>() {
            protected Client initialValue()
            {
                AesClient aesClient = new AesClient(publicKey, RpcResult.getInstance());
                aesClient.setIp(ip).setPort(port).setReadTimeout(readTimeout).setReuseChannelTimeout(reuseChannelTimeout).setDataFilters(AesCodeInvoker.this.dataFilters).setHeadFactory(headFactory);
                return aesClient;
            }
        };
        return this;
    }
    
    public AesCodeInvoker setPort(final int port)
    {
        this.port = port;
        clientChanelLocal = new ThreadLocal<Client>() {
            protected Client initialValue()
            {
                AesClient aesClient = new AesClient(publicKey, RpcResult.getInstance());
                aesClient.setIp(ip).setPort(port).setReadTimeout(readTimeout).setReuseChannelTimeout(reuseChannelTimeout).setDataFilters(AesCodeInvoker.this.dataFilters).setHeadFactory(headFactory);
                return aesClient;
            }
        };
        return this;
    }
    
    public AesCodeInvoker setHeadFactory(final HeadFactory headFactory)
    {
        this.headFactory = headFactory;
        clientChanelLocal = new ThreadLocal<Client>() {
            protected Client initialValue()
            {
                AesClient aesClient = new AesClient(publicKey, RpcResult.getInstance());
                aesClient.setIp(ip).setPort(port).setReadTimeout(readTimeout).setReuseChannelTimeout(reuseChannelTimeout).setDataFilters(AesCodeInvoker.this.dataFilters).setHeadFactory(headFactory);
                return aesClient;
            }
        };
        return this;
    }
}
