package link.jfire.simplerpc.client;

import link.jfire.socket.socketclient.filter.DataFilter;
import link.jfire.socket.socketserver.util.DefaultHeadFactory;
import link.jfire.socket.socketserver.util.HeadFactory;

public class RpcProxyConfig<T>
{
    private String       proxyName;
    private String       ip;
    private int          port;
    private DataFilter[] dataFilters         = new DataFilter[0];
    private long         readTimeout         = 3000;
    private long         reuseChannelTimeout = 55000;
    private Class<T>     interfaceClass;
    private byte[]       publicKey;
    private HeadFactory  headFactory         = new DefaultHeadFactory();
    
    public RpcProxyConfig(Class<T> interfaceClass)
    {
        this.interfaceClass = interfaceClass;
    }
    
    public T getProxy()
    {
        checkProxyParams();
        BytecodeInvoker bytecodeInvoker = new BytecodeInvoker();
        bytecodeInvoker.setIp(ip).setPort(port).setProxyName(proxyName)
                .setReadTimeout(readTimeout).setReuseChannelTimeout(reuseChannelTimeout)
                .setDataFilters(dataFilters).setHeadFactory(headFactory);
        return RpcFactory.getProxy(interfaceClass, bytecodeInvoker);
    }
    
    private void checkProxyParams()
    {
        if (ip == null)
        {
            throw new RuntimeException("请设置服务端ip");
        }
        if (port < 0 || port > 62255)
        {
            throw new RuntimeException("请设置服务端端口");
        }
        
    }
    
    private void checkAuthParam()
    {
        checkProxyParams();
        if (publicKey == null)
        {
            throw new RuntimeException("未设置客户端公钥");
        }
    }
    
    public T getAesProxy()
    {
        checkAuthParam();
        AesCodeInvoker aesCodeInvoker = new AesCodeInvoker(publicKey);
        aesCodeInvoker.setIp(ip).setPort(port).setProxyName(proxyName)
                .setReadTimeout(readTimeout).setReuseChannelTimeout(reuseChannelTimeout)
                .setDataFilters(dataFilters).setHeadFactory(headFactory);
        return RpcFactory.getProxy(interfaceClass, aesCodeInvoker);
    }
    
    public RpcProxyConfig<T> setProxyName(String proxyName)
    {
        this.proxyName = proxyName;
        return this;
    }
    
    public RpcProxyConfig<T> setIp(String ip)
    {
        this.ip = ip;
        return this;
    }
    
    public RpcProxyConfig<T> setPort(int port)
    {
        this.port = port;
        return this;
    }
    
    public RpcProxyConfig<T> setDataFilters(DataFilter[] dataFilters)
    {
        this.dataFilters = dataFilters;
        return this;
    }
    
    public RpcProxyConfig<T> setReadTimeout(long readTimeout)
    {
        this.readTimeout = readTimeout;
        return this;
    }
    
    public RpcProxyConfig<T> setReuseChannelTimeout(long reuseChannelTimeout)
    {
        this.reuseChannelTimeout = reuseChannelTimeout;
        return this;
    }
    
    public RpcProxyConfig<T> setPublicKey(byte[] publicKey)
    {
        this.publicKey = publicKey;
        return this;
    }
    
    public RpcProxyConfig<T> setHeadFactory(HeadFactory headFactory)
    {
        this.headFactory = headFactory;
        return this;
    }
}
