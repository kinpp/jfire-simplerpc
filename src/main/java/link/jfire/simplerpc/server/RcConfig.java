package link.jfire.simplerpc.server;

import link.jfire.socket.socketserver.transfer.server.ServerConfig;

public class RcConfig extends ServerConfig
{
    private String[] proxyNames;
    private Object[] impls;
    
    public String[] getProxyNames()
    {
        return proxyNames;
    }
    
    public void setProxyNames(String... proxyNames)
    {
        this.proxyNames = proxyNames;
    }
    
    public Object[] getImpls()
    {
        return impls;
    }
    
    public void setImpls(Object... impls)
    {
        this.impls = impls;
    }
    
    /**
     * 简单判断参数是否正常，主要是判断不为空，并且代理名称和实现的个数相同
     * 
     * @return
     */
    public boolean isParamOk()
    {
        if (proxyNames != null && proxyNames.length > 0 && impls != null && impls.length > 0)
        {
            if (proxyNames.length == impls.length)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
