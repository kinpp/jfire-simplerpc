package link.jfire.simplerpc.server.messagehandler;

public class InvokeType
{
    public static byte METHOD_INVOKE = 0x32;
    public static byte METHOD_INFO   = 0x33;
}
