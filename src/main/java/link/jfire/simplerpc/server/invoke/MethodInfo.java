package link.jfire.simplerpc.server.invoke;

import java.lang.reflect.Method;
import link.jfire.baseutil.collection.set.LightSet;
import link.jfire.simplerpc.annotation.MethodName;

public class MethodInfo
{
    private String methodName;
    private String realMethod;
    private int    argNum;
    
    public MethodInfo(Method method)
    {
        methodName = method.getAnnotation(MethodName.class).value();
        realMethod = method.getName();
        argNum = method.getParameterTypes().length;
        if (argNum == 0)
        {
            return;
        }
        Class<?>[] types = method.getParameterTypes();
        LightSet<String> set = new LightSet<>();
    }
    
    private String getParamName(Class<?> type)
    {
        return null;
    }
}
