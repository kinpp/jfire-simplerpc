package link.jfire.simplerpc.server.invoke;

public interface InvokeProxy
{
    public Object invoke(String methodName, Object[] args);
}
