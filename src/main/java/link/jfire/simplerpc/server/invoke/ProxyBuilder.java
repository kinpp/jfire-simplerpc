package link.jfire.simplerpc.server.invoke;

import java.lang.reflect.Method;
import javassist.ClassClassPath;
import javassist.ClassPool;
import link.jfire.baseutil.collection.set.LightSet;
import link.jfire.baseutil.reflect.ReflectUtil;
import link.jfire.baseutil.verify.Verify;
import link.jfire.simplerpc.annotation.MethodName;
import link.jfire.simplerpc.annotation.ProxyName;

public class ProxyBuilder
{
    private static ClassPool classPool = ClassPool.getDefault();
    static
    {
        ClassPool.doPruning = true;
        classPool.insertClassPath(new ClassClassPath(ProxyBuilder.class));
        classPool.importPackage("link.jfire.socket");
        classPool.importPackage("link.jfire.simplerpc");
        classPool.importPackage("java.util");
    }
    
    public static InvokeProxy build(Class<?> type)
    {
        ProxyName proxyName = type.getAnnotation(ProxyName.class);
        Verify.exist(proxyName, "类{}没有ProxyName注解", type);
        Method[] methods = getMethods(type);
        Verify.True(methods.length > 0, "类{}没有注解了MethodName的方法", type);
        return null;
    }
    
    /**
     * 得到所有打上了MethodName注解的方法
     * 
     * @param type
     * @return
     */
    private static Method[] getMethods(Class<?> type)
    {
        Method[] methods = ReflectUtil.getAllMehtods(type);
        LightSet<Method> set = new LightSet<>();
        for (Method each : methods)
        {
            if (each.isAnnotationPresent(MethodName.class))
            {
                set.add(each);
            }
        }
        return set.toArray(Method.class);
    }
}
